import os
import textstat
import pygubu
import tkinter as tk
import ReadDoc.readfile as readfile


#class for building the Tk window
class Application:
    def __init__(self, master):
        self.master = master

        #building the widgets: main window
        self.builder = builder = pygubu.Builder()
        builder.add_from_file('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/read_doc.ui')
        self.mainwindow = builder.get_object('mainwindow', master)

        #building the widgets: other
        self.filePath = builder.get_object('filePath', master)

        #connecting the methods below to the widgets above
        builder.connect_callbacks(self)


        #methods go here:
    def on_clicked_quit(self):
        self.master.quit()


    def on_clicked_calc_fk_ease(self):
        textFile = self.filePath.get()
        outputFile = open('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt', mode='w')
        text = readfile.read_text(textFile)

        for line in range(len(text)):
            outputFile.write("F-K Ease at paragraph " + str(line + 1) + ": " + str(textstat.flesch_reading_ease(text[line])) + "\n")
        outputFile.close()

        os.startfile(textFile)
        os.startfile('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt')


    def on_clicked_sentence_decomp(self):
        textFile = self.filePath.get()
        outputFile = open('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt', mode='w')
        text = readfile.read_text(textFile)
        textSentence = readfile.sentence_decomp(text)

        for line in range(len(textSentence)):
            outputFile.write("F-K Ease at line " + str(line + 1) + ": " + str(textstat.flesch_reading_ease(textSentence[line])) + "\n")
        outputFile.close()

        os.startfile(textFile)
        os.startfile('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt')



    def on_clicked_full_page(self):
        textFile = self.filePath.get()
        outputFile = open('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt', mode='w')
        text = readfile.read_text(textFile)
        textFullPage = readfile.full_page(text)

        outputFile.write("F-k Ease: " + str(textstat.flesch_reading_ease(textFullPage)) + "\n")
        outputFile.close()

        os.startfile(textFile)
        os.startfile('C:/Users/mlryc/PycharmProjects/geometryreadabilityproject/ReadDoc/output.txt')


#main code automatically executes:
if __name__ == '__main__':
    root = tk.Tk()
    root.title("GUI for textstat Readability")
    myApp = Application(root)

    root.mainloop()
