# Functions from reading in text files and using the textstat package to analysze them.
import textstat


def read_text(filePath):
    '''
    :param filePath: Any string that is a path to the text file to be analyzed.
    :return: An array whose elements are lines of text.
    '''
    with open(filePath, mode='r') as textFile:
        textList = textFile.readlines()

    textFile.close()

    for line in range(len(textList)):
        textList[line] = textList[line].rstrip()

    return textList


def sentence_decomp(textArray):
    '''
    :param textArray: An array whose elements are lines of text.
    :return: A new array in which each element is only one sentence.
    (e.g. [['This is a sentence.', [This is another sentence.']])
    '''
    fullPage = full_page(textArray)
    periodPosList = [0]
    tempList = get_period_pos(fullPage)
    
    for i in range(len(tempList)):
        periodPosList.append(tempList[i])
    #print(periodPosList)

    sentenceArray = []
    index = 0

    while index < (len(periodPosList) - 1):
        sentenceArray.append(fullPage[periodPosList[index]:periodPosList[index + 1]])
        index += 1

    return sentenceArray


def full_page(textArray):
    '''
    :param textArray: Any array whose elements are lines of text.
    :return: A one-dimensional array whose only element is a concatenation of all the elements in textArray.
    '''
    index = 0
    concatList = ''

    while index < len(textArray):
        concatList = concatList + textArray[index]
        index += 1

    return concatList


def get_period_pos(line):
    '''
    :param line: Any string of text.
    :return: An array that contains the position of each period '.' in the string of text.
    '''
    index = 0
    periodPos = []

    while index < len(line):
        if line[index] == '.':
            periodPos.append(index + 1)
        index += 1

    return periodPos
