Introduction to The Geometry Readability Project and ReadDoc GUI 

    The aim of the Geometry Readability Project, and the ReadDoc GUI, is to produce numerical data that
    helps us compare the 'readability' of Isaac Barrow's 1732 edition of "Euclide's Elements" with other
    mathematics and science texts from a the same bookshops (in London), same newspaper advirtisement
    sections, or, more generally, the time period (early- to mid-eighteenth century).
    
    Determining the actual contemporary readers of these texts is often quite difficult or nearly
    impossible due to lack of evidence.  Therefore, determining who was likely to have constituted the 
    general audience for these books by saying something about the readability of the texts will give us
    at least some insight.
    
    The software's utility relies on the textstat package for Python.  This package contains several
    different readability analysis tools.  The one primarily used for this project is the
    Flesch-Kincaid Ease Scale.



Section A: Instructions for Formatting "Euclide's Elements" Pages

    The following instructions explain how to manually copy and format and a plain text file with conent 
    from a page of Isaac Barrow's 1732 edition of "Eulcide's Element" for analysis with the ReadDoc GUI.
    
        1.  All text should be in UTF-8 format and saved with the '.txt' extension.  Common text editing 
            software packages that do this automatically are NotePad (Windows), gedit (various 
            Linux distributions), and TextEdit (MacOS). Note: ReadDoc GUI has not been tested with other 
            file formats.
         
        2.  Images of pages that need to be copied into plain text format can be found in the 
            'ReadDoc\docs\barrow\img' directory.
         
        3.  Save completed plain text files to 'ReadDoc\docs\barrow' with the name of the file in the
            following format: 'book[book number]_page[page number].txt' (enter a number and delete 
            the brackets).  For example:
            
                Book 1 of Elements, page 8 becomes ---> book1_page8.txt
            
        4.  Each paragrapgh is to be entered on its own line.  There should be no spaces between lines.
            For example:
            
                "This is a paragraph.  It contains some sentences.  They're on the same line.
                 Here's a new paragraph.  It's on a new line."
                 
            Do no include the quotation marks, unless they are already part of the text.
            


Section B: Instructions for Formatting Other Texts for Comparative Analysis